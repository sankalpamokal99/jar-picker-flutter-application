import 'package:data_connection_checker/data_connection_checker.dart';

class InternetConnection {
  Future<bool> checkInternet() async {
    bool result;
    try {
      result = await DataConnectionChecker().hasConnection;
      return result;
    } catch (e) {
      print(e.toString());
      result = false;
      return result;
    }
  }
}
