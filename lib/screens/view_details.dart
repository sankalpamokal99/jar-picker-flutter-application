import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:jar_picker/provider/internet_connection.dart';
import 'package:jar_picker/widgets/alert_box.dart';

class ViewDetails extends StatefulWidget {
  @override
  _ViewDetailsState createState() => _ViewDetailsState();
}

class _ViewDetailsState extends State<ViewDetails> {
  List<DocumentSnapshot> customers = <DocumentSnapshot>[];
  List<DropdownMenuItem<String>> customersDropDown =
      <DropdownMenuItem<String>>[];
  String _currentCustomer;
  final _formKey = GlobalKey<FormState>();
  DateTime selectedDate = DateTime.now();
  var selectedDateString =
      DateFormat("dd-MM-yyyy").format(DateTime.now()).toString();
  var _exists;
  int _price;
  int _overallAmountRemaining;
  int _overallJarsRemaining;

  AlertBox _alertBox = AlertBox();
  InternetConnection ic = InternetConnection();
  Firestore _firestore = Firestore.instance;

  @override
  void initState() {
    super.initState();
    _getCustomers();
  }

  _getCustomers() async {
    List<DocumentSnapshot> data = await getCustomers();
    setState(() {
      customers = data;
      customersDropDown = getCustomersDropdown();
      _currentCustomer = customers[0].data['cid'];
    });
  }

  Future<List<DocumentSnapshot>> getCustomers() =>
      _firestore.collection("customers").getDocuments().then((snaps) {
        return snaps.documents;
      });

  List<DropdownMenuItem<String>> getCustomersDropdown() {
    List<DropdownMenuItem<String>> items = new List();
    for (int i = 0; i < customers.length; i++) {
      setState(() {
        items.insert(
            0,
            DropdownMenuItem(
                child: Text(customers[i].data['cname']),
                value: customers[i].data['cid']));
      });
    }
    return items;
  }

  changeSelectedCustomer(String selectedCustomer) {
    setState(() {
      return _currentCustomer = selectedCustomer;
    });
    print(_currentCustomer);
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime.now());
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDateString = DateFormat("dd-MM-yyyy").format(picked).toString();
      });
  }

  _viewDetails() {
    setState(() {
      _exists = checkIfExists();
      getCustomerDetails();
    });
  }

  Future<DocumentSnapshot> checkIfExists() async {
    return await _firestore
        .collection('customers')
        .document(_currentCustomer)
        .collection('orderdata')
        .document(selectedDateString)
        .get();
  }

  getCustomerDetails() async {
    await _firestore
        .collection('customers')
        .document(_currentCustomer)
        .get()
        .then((snapshot) {
      _price = snapshot.data['cprice'];
      _overallJarsRemaining = snapshot.data['overall_jars_remaining'];
      _overallAmountRemaining = snapshot.data['overall_pending_amount'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("View Customer Details"),
        ),
        body: Stack(
          children: <Widget>[
            Form(
                key: _formKey,
                child: ListView(
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.fromLTRB(10.0, 20.0, 15.0, 10.0),
                      child: Container(
                        decoration: ShapeDecoration(
                          color: Colors.grey.withOpacity(0.2),
                          shape: RoundedRectangleBorder(
                            side: BorderSide(style: BorderStyle.solid),
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                          ),
                        ),
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  10.0, 0.0, 15.0, 10.0),
                              child: Icon(
                                Icons.person_outline,
                                color: Colors.grey,
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 15.0, 10.0),
                                  child: DropdownButton(
                                    underline: SizedBox.shrink(),
                                    items: customersDropDown,
                                    onChanged: changeSelectedCustomer,
                                    value: _currentCustomer,
                                  )),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.fromLTRB(10.0, 20.0, 15.0, 10.0),
                      child: Container(
                        decoration: ShapeDecoration(
                          color: Colors.grey.withOpacity(0.2),
                          shape: RoundedRectangleBorder(
                            side: BorderSide(style: BorderStyle.solid),
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                          ),
                        ),
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  10.0, 0.0, 15.0, 10.0),
                              child: Icon(
                                Icons.calendar_today,
                                color: Colors.grey,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  0.0, 0.0, 15.0, 10.0),
                              child: RaisedButton(
                                color: Colors.grey.withOpacity(0.0),
                                elevation: 0.0,
                                onPressed: () => _selectDate(context),
                                child: Text(
                                  selectedDateString,
                                  style: TextStyle(fontSize: 20.0),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(80.0, 8.0, 80.0, 8.0),
                      child: SizedBox(
                        width: 120.0,
                        height: 50.0,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          color: Colors.black,
                          textColor: Colors.white,
                          onPressed: () async {
                            if (await ic.checkInternet()) {
                              _viewDetails();
                            } else {
                              _alertBox.showAlertDialog(context, "No Internet",
                                  "Check your internet connection!");
                            }
                          },
                          child: const Text('View Details',
                              style: TextStyle(fontSize: 20)),
                        ),
                      ),
                    ),
                    FutureBuilder<DocumentSnapshot>(
                        future: _exists,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (snapshot.data.exists) {
                              var data = snapshot.data;
                              return Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 20.0),
                                        child: RichText(
                                          text: TextSpan(
                                            text: 'Rate : ',
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                color: Colors.black),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text: "$_price",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              TextSpan(text: ' /Jar'),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 20.0),
                                        child: RichText(
                                          text: TextSpan(
                                            text: 'Jars Bought : ',
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                color: Colors.black),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text:
                                                      "${data['jars_bought']}",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 20.0),
                                        child: RichText(
                                          text: TextSpan(
                                            text: 'Jars Returned : ',
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                color: Colors.black),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text:
                                                      "${data['jars_returned']}",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 20.0),
                                        child: RichText(
                                          text: TextSpan(
                                            text: 'Amount Paid : ',
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                color: Colors.black),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text:
                                                      "${data['payment_done']}",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 20.0),
                                        child: RichText(
                                          text: TextSpan(
                                            text: 'Overall Pending Jars : ',
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                color: Colors.black),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text:
                                                      "$_overallJarsRemaining",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 20.0),
                                        child: RichText(
                                          text: TextSpan(
                                            text: 'Overall Pending Payment : ',
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                color: Colors.black),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text:
                                                      "$_overallAmountRemaining",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            } else {
                              return Center(
                                  child: Container(
                                      child: Text("Data not found",
                                          style: TextStyle(fontSize: 20.0))));
                            }
                          } else {
                            return SizedBox.shrink();
                          }
                        })
                  ],
                ))
          ],
        ));
  }
}
