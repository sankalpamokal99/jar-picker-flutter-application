import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:jar_picker/provider/internet_connection.dart';
import 'package:jar_picker/widgets/alert_box.dart';
import 'dart:async';
import 'package:uuid/uuid.dart';

class AddCustomer extends StatefulWidget {
  @override
  _AddCustomerState createState() => _AddCustomerState();
}

class _AddCustomerState extends State<AddCustomer> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _mobileController = TextEditingController();
  TextEditingController _priceController = TextEditingController();
  TextEditingController _jarsPendingController = TextEditingController();
  TextEditingController _payRemainingController = TextEditingController();

  Firestore _firestore = Firestore.instance;

  final _formKey = GlobalKey<FormState>();
  InternetConnection ic = InternetConnection();
  AlertBox _alertBox = AlertBox();

  Future<bool> addCustomer(Map<String, dynamic> data) async {
    String customerId = Uuid().v1();
    data['cid'] = customerId;
    try {
      await _firestore
          .collection('customers')
          .document(customerId)
          .setData(data);
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Customer"),
      ),
      body: Stack(
        children: <Widget>[
          Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 20.0, 15.0, 10.0),
                    child: TextFormField(
                      controller: _nameController,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.person_outline),
                          fillColor: Colors.grey.withOpacity(0.2),
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                            const Radius.circular(8.0),
                          )),
                          hintText: "Enter Customer Name"),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "The name field cannot be empty";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 15.0, 15.0, 10.0),
                    child: TextFormField(
                      controller: _emailController,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.alternate_email),
                        fillColor: Colors.grey.withOpacity(0.2),
                        filled: true,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                          const Radius.circular(8.0),
                        )),
                        hintText: "Enter Customer Email",
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          Pattern pattern =
                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          RegExp regex = new RegExp(pattern);
                          if (!regex.hasMatch(value))
                            return 'Please make sure your email address is valid';
                          else
                            return null;
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 15.0, 15.0, 10.0),
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      controller: _mobileController,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.phone_android),
                          fillColor: Colors.grey.withOpacity(0.2),
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                            const Radius.circular(8.0),
                          )),
                          hintText: "Enter Customer Mobile"),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Mobile number is required";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 15.0, 15.0, 10.0),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      controller: _priceController,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.attach_money),
                          fillColor: Colors.grey.withOpacity(0.2),
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                            const Radius.circular(8.0),
                          )),
                          hintText: "Enter Price/Jar"),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Price is required";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 15.0, 15.0, 10.0),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      controller: _payRemainingController,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.attach_money),
                          fillColor: Colors.grey.withOpacity(0.2),
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                            const Radius.circular(8.0),
                          )),
                          hintText: "Enter Remaining Payment"),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Enter 0 if no remaining payment";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 15.0, 15.0, 10.0),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      controller: _jarsPendingController,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.refresh),
                          fillColor: Colors.grey.withOpacity(0.2),
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                            const Radius.circular(8.0),
                          )),
                          hintText: "Enter Jars Pending"),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Enter 0 if no remaining jars to return";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(80.0, 8.0, 80.0, 8.0),
                    child: SizedBox(
                      width: 120.0,
                      height: 50.0,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        color: Colors.black,
                        textColor: Colors.white,
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            if (await ic.checkInternet()) {
                              if (!await addCustomer({
                                "cname": _nameController.text,
                                "cemail": _emailController.text,
                                "cmobile": int.parse(_mobileController.text),
                                "cprice": int.parse(_priceController.text),
                                "overall_pending_amount":
                                    int.parse(_payRemainingController.text),
                                "overall_jars_remaining":
                                    int.parse(_jarsPendingController.text)
                              })) {
                                _formKey.currentState.reset();
                                _alertBox.showAlertDialog(
                                    context, "Failed", "Something went exists");
                              }
                              _formKey.currentState.reset();
                              _alertBox.showAlertDialog(
                                  context, "Success", "Customer Added");
                            } else {
                              _alertBox.showAlertDialog(context, "No Internet",
                                  "Check your internet connection!");
                            }
                          }
                        },
                        child: const Text('Submit',
                            style: TextStyle(fontSize: 20)),
                      ),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
