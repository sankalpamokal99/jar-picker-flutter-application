import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:jar_picker/provider/internet_connection.dart';
import 'dart:async';

import 'package:jar_picker/widgets/alert_box.dart';

class AddDailyData extends StatefulWidget {
  @override
  _AddDailyDataState createState() => _AddDailyDataState();
}

class _AddDailyDataState extends State<AddDailyData> {
  List<DocumentSnapshot> customers = <DocumentSnapshot>[];
  List<DropdownMenuItem<String>> customersDropDown =
      <DropdownMenuItem<String>>[];
  String _currentCustomer;
  int _rate;
  int _overallJarsRemaining;
  int _overallAmountRemaining;

  int _monthlyJarsBought;
  int _monthlyJarsReturned;
  int _monthlyPaymentExpected;
  int _monthlyPaymentDone;

  final _formKey = GlobalKey<FormState>();
  TextEditingController _jboughtController = TextEditingController();
  TextEditingController _jreturnedController = TextEditingController();
  TextEditingController _payController = TextEditingController();
  var dateToday = DateFormat("dd-MM-yyyy").format(DateTime.now()).toString();
  var monthYear = DateFormat("MM-yyyy").format(DateTime.now()).toString();

  AlertBox _alertBox = AlertBox();
  InternetConnection ic = InternetConnection();
  Firestore _firestore = Firestore.instance;

  @override
  void initState() {
    super.initState();
    _getCustomers();
    print(monthYear);
  }

  _getCustomers() async {
    List<DocumentSnapshot> data = await getCustomers();
    setState(() {
      customers = data;
      customersDropDown = getCustomersDropdown();
      _currentCustomer = customers[0].data['cid'];
    });
  }

  Future<List<DocumentSnapshot>> getCustomers() =>
      _firestore.collection("customers").getDocuments().then((snaps) {
        return snaps.documents;
      });

  List<DropdownMenuItem<String>> getCustomersDropdown() {
    List<DropdownMenuItem<String>> items = new List();
    for (int i = 0; i < customers.length; i++) {
      setState(() {
        items.insert(
            0,
            DropdownMenuItem(
                child: Text(customers[i].data['cname']),
                value: customers[i].data['cid']));
      });
    }
    return items;
  }

  changeSelectedCustomer(String selectedCustomer) {
    setState(() {
      _currentCustomer = selectedCustomer;
      getCustomerDetails();
      return _currentCustomer;
    });
    print(_currentCustomer);
  }

  Future<bool> addDailyData(Map<String, dynamic> data) async {
    String customerId = data['cid'];
    bool flag;
    try {
      await _firestore
          .collection('customers')
          .document(customerId)
          .collection("orderdata")
          .document(dateToday)
          .get()
          .then((snapshot) async {
        if (snapshot.exists) {
          flag = false;
        } else {
          await _firestore
              .collection('customers')
              .document(customerId)
              .collection("orderdata")
              .document(dateToday)
              .setData(data);

          _overallJarsRemaining = _overallJarsRemaining -
              int.parse(_jreturnedController.text) +
              int.parse(_jboughtController.text);
          _overallAmountRemaining = _overallAmountRemaining -
              int.parse(_payController.text) +
              (int.parse(_jboughtController.text) * _rate);

          await _firestore
              .collection('customers')
              .document(customerId)
              .updateData({
            'overall_jars_remaining': _overallJarsRemaining,
            'overall_pending_amount': _overallAmountRemaining
          });

          flag = await addMonthlyData();
        }
      });
      return flag;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  getCustomerDetails() async {
    await _firestore
        .collection('customers')
        .document(_currentCustomer)
        .get()
        .then((snapshot) {
      _rate = snapshot.data['cprice'];
      _overallJarsRemaining = snapshot.data['overall_jars_remaining'];
      _overallAmountRemaining = snapshot.data['overall_pending_amount'];
    });
  }

  Future<bool> addMonthlyData() async {
    bool flag;
    try {
      await _firestore
          .collection('customers')
          .document(_currentCustomer)
          .collection("monthlydata")
          .document(monthYear)
          .get()
          .then((snapshot) async {
        if (snapshot.exists) {
          _monthlyJarsBought = snapshot.data['monthly_jars_bought'];
          _monthlyJarsReturned = snapshot.data['monthly_jars_returned'];
          _monthlyPaymentExpected = snapshot.data['monthly_payment_expected'];
          _monthlyPaymentDone = snapshot.data['monthly_payment_done'];

          _monthlyJarsBought =
              _monthlyJarsBought + int.parse(_jboughtController.text);
          _monthlyJarsReturned =
              _monthlyJarsReturned + int.parse(_jreturnedController.text);
          _monthlyPaymentExpected = _monthlyPaymentExpected +
              (int.parse(_jboughtController.text) * _rate);
          _monthlyPaymentDone =
              _monthlyPaymentDone + int.parse(_payController.text);
          flag = true;
        } else {
          await _firestore
              .collection('customers')
              .document(_currentCustomer)
              .collection("orderdata")
              .document(monthYear)
              .setData({
            "monthly_jars_bought": int.parse(_jboughtController.text),
            "monthly_jars_returned": int.parse(_jreturnedController.text),
            "monthly_payment_expected":
                int.parse(_jboughtController.text) * _rate,
            "monthly_payment_done": int.parse(_payController.text)
          });
          flag = true;
        }
      });
      return flag;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Data"),
      ),
      body: Stack(
        children: <Widget>[
          Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Center(
                        child: Text(
                      dateToday,
                      style: TextStyle(fontSize: 25.0),
                    )),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 20.0, 15.0, 10.0),
                    child: Container(
                      decoration: ShapeDecoration(
                        color: Colors.grey.withOpacity(0.2),
                        shape: RoundedRectangleBorder(
                          side: BorderSide(style: BorderStyle.solid),
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                      ),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(
                                10.0, 0.0, 15.0, 10.0),
                            child: Icon(
                              Icons.person_outline,
                              color: Colors.grey,
                            ),
                          ),
                          Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    0.0, 0.0, 15.0, 10.0),
                                child: DropdownButton(
                                  underline: SizedBox.shrink(),
                                  items: customersDropDown,
                                  onChanged: changeSelectedCustomer,
                                  value: _currentCustomer,
                                )),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 20.0, 15.0, 10.0),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      controller: _jboughtController,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.shopping_cart),
                          fillColor: Colors.grey.withOpacity(0.2),
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                            const Radius.circular(8.0),
                          )),
                          hintText: "Enter Jars Bought"),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "This field cannot be empty";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 20.0, 15.0, 10.0),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      controller: _jreturnedController,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.remove_shopping_cart),
                          fillColor: Colors.grey.withOpacity(0.2),
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                            const Radius.circular(8.0),
                          )),
                          hintText: "Enter Jars Returned"),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "This field cannot be empty";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 20.0, 15.0, 10.0),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      controller: _payController,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.attach_money),
                          fillColor: Colors.grey.withOpacity(0.2),
                          filled: true,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                            const Radius.circular(8.0),
                          )),
                          hintText: "Enter Payment Done"),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "This field cannot be empty";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(80.0, 8.0, 80.0, 8.0),
                    child: SizedBox(
                      width: 120.0,
                      height: 50.0,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        color: Colors.black,
                        textColor: Colors.white,
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            if (await ic.checkInternet()) {
                              if (!await addDailyData({
                                "cid": _currentCustomer,
                                "jars_bought":
                                    int.parse(_jboughtController.text),
                                "jars_returned":
                                    int.parse(_jreturnedController.text),
                                "payment_done": int.parse(_payController.text),
                                "daily_charge":
                                    int.parse(_jboughtController.text) * _rate
                              })) {
                                _formKey.currentState.reset();
                                _alertBox.showAlertDialog(
                                    context, "Failed", "Data already exists");
                              }
                              _alertBox.showAlertDialog(context, "Success",
                                  "Data Added Successfully! Today's Charge is ${int.parse(_jboughtController.text) * _rate}");
                              _formKey.currentState.reset();
                            }
                          } else {
                            _alertBox.showAlertDialog(context, "No Internet",
                                "Check your internet connection!");
                          }
                        },
                        child: const Text('Submit',
                            style: TextStyle(fontSize: 20)),
                      ),
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
