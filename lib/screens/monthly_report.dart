import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:jar_picker/provider/internet_connection.dart';
import 'package:jar_picker/widgets/alert_box.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';

class MonthlyReport extends StatefulWidget {
  @override
  _MonthlyReportState createState() => _MonthlyReportState();
}

class _MonthlyReportState extends State<MonthlyReport> {
  List<DocumentSnapshot> customers = <DocumentSnapshot>[];
  List<DropdownMenuItem<String>> customersDropDown =
      <DropdownMenuItem<String>>[];
  String _currentCustomer;
  var _exists;
  AlertBox _alertBox = AlertBox();
  InternetConnection ic = InternetConnection();
  Firestore _firestore = Firestore.instance;

  DateTime selectedDate = DateTime.now();
  var selectedDateString =
      DateFormat("MM-yyyy").format(DateTime.now()).toString();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showMonthPicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(DateTime.now().year - 2, 5),
        lastDate: DateTime(DateTime.now().year, DateTime.now().month));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDateString = DateFormat("MM-yyyy").format(picked).toString();
        print(selectedDateString);
      });
  }

  @override
  void initState() {
    super.initState();
    _getCustomers();
  }

  _getCustomers() async {
    List<DocumentSnapshot> data = await getCustomers();
    setState(() {
      customers = data;
      customersDropDown = getCustomersDropdown();
      _currentCustomer = customers[0].data['cid'];
    });
  }

  Future<List<DocumentSnapshot>> getCustomers() =>
      _firestore.collection("customers").getDocuments().then((snaps) {
        return snaps.documents;
      });

  List<DropdownMenuItem<String>> getCustomersDropdown() {
    List<DropdownMenuItem<String>> items = new List();
    for (int i = 0; i < customers.length; i++) {
      setState(() {
        items.insert(
            0,
            DropdownMenuItem(
                child: Text(customers[i].data['cname']),
                value: customers[i].data['cid']));
      });
    }
    return items;
  }

  changeSelectedCustomer(String selectedCustomer) {
    setState(() {
      return _currentCustomer = selectedCustomer;
    });
    print(_currentCustomer);
  }

  _viewDetails() {
    setState(() {
      _exists = checkIfExists();
    });
  }

  Future<DocumentSnapshot> checkIfExists() async {
    return await _firestore
        .collection('customers')
        .document(_currentCustomer)
        .collection('orderdata')
        .document(selectedDateString)
        .get();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Monthly Report"),
      ),
      body: Stack(
        children: <Widget>[
          Form(
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 20.0, 15.0, 10.0),
                  child: Container(
                    decoration: ShapeDecoration(
                      color: Colors.grey.withOpacity(0.2),
                      shape: RoundedRectangleBorder(
                        side: BorderSide(style: BorderStyle.solid),
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(10.0, 0.0, 15.0, 10.0),
                          child: Icon(
                            Icons.person_outline,
                            color: Colors.grey,
                          ),
                        ),
                        Expanded(
                          child: Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  0.0, 0.0, 15.0, 10.0),
                              child: DropdownButton(
                                underline: SizedBox.shrink(),
                                items: customersDropDown,
                                onChanged: changeSelectedCustomer,
                                value: _currentCustomer,
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 20.0, 15.0, 10.0),
                  child: Container(
                    decoration: ShapeDecoration(
                      color: Colors.grey.withOpacity(0.2),
                      shape: RoundedRectangleBorder(
                        side: BorderSide(style: BorderStyle.solid),
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(10.0, 0.0, 15.0, 10.0),
                          child: Icon(
                            Icons.calendar_today,
                            color: Colors.grey,
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(0.0, 0.0, 15.0, 10.0),
                          child: RaisedButton(
                            color: Colors.grey.withOpacity(0.0),
                            elevation: 0.0,
                            onPressed: () => _selectDate(context),
                            child: Text(
                              selectedDateString,
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(80.0, 8.0, 80.0, 8.0),
                  child: SizedBox(
                    width: 120.0,
                    height: 50.0,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      color: Colors.black,
                      textColor: Colors.white,
                      onPressed: () async {
                        if (await ic.checkInternet()) {
                          _viewDetails();
                        } else {
                          _alertBox.showAlertDialog(context, "No Internet",
                              "Check your internet connection!");
                        }
                      },
                      child: const Text('Get Report',
                          style: TextStyle(fontSize: 20)),
                    ),
                  ),
                ),
                FutureBuilder<DocumentSnapshot>(
                    future: _exists,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.data.exists) {
                          var data = snapshot.data;
                          return Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top: 20.0),
                                    child: RichText(
                                      text: TextSpan(
                                        text: 'Monthly Jars Bought : ',
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.black),
                                        children: <TextSpan>[
                                          TextSpan(
                                              text:
                                                  "${data['monthly_jars_bought']}",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 20.0),
                                    child: RichText(
                                      text: TextSpan(
                                        text: 'Monthly Jars Returned : ',
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.black),
                                        children: <TextSpan>[
                                          TextSpan(
                                              text:
                                                  "${data['monthly_jars_returned']}",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 20.0),
                                    child: RichText(
                                      text: TextSpan(
                                        text: 'Monthly Payment Expected : ',
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.black),
                                        children: <TextSpan>[
                                          TextSpan(
                                              text:
                                                  "${data['monthly_payment_expected']}",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 20.0),
                                    child: RichText(
                                      text: TextSpan(
                                        text: 'Monthly Payment Done : ',
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.black),
                                        children: <TextSpan>[
                                          TextSpan(
                                              text:
                                                  "${data['monthly_payment_done']}",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        } else {
                          return Center(
                              child: Container(
                                  child: Text("Data not found",
                                      style: TextStyle(fontSize: 20.0))));
                        }
                      } else {
                        return SizedBox.shrink();
                      }
                    })
              ],
            ),
          ),
        ],
      ),
    );
  }
}
