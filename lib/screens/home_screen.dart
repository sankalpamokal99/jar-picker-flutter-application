import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:jar_picker/provider/internet_connection.dart';
import 'package:jar_picker/provider/user_provider.dart';
import 'package:jar_picker/screens/add_customer.dart';
import 'package:jar_picker/screens/add_daily_data.dart';
import 'package:jar_picker/screens/login_screen.dart';
import 'package:jar_picker/screens/reports_screen.dart';
import 'package:jar_picker/screens/view_details.dart';
import 'package:jar_picker/widgets/alert_box.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var access;
  InternetConnection ic = InternetConnection();
  AlertBox _alertBox = AlertBox();
  Firestore _firestore = Firestore.instance;

  @override
  Widget build(BuildContext context) {
    UserProvider user = Provider.of<UserProvider>(context);

    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          title: Text("Jar Picker"),
        ),
        drawer: Drawer(
          child: StreamBuilder(
            stream: _firestore
                .collection('users')
                .document(user.user.uid)
                .snapshots(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var userData = snapshot.data;
                access = userData["role"];
                print(access);
                print(userData["name"]);
                return ListView(
                  children: <Widget>[
                    // Drawer Header
                    UserAccountsDrawerHeader(
                        accountName: Text(userData["name"]),
                        accountEmail: Text(userData["email"]),
                        currentAccountPicture: CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Icon(Icons.person),
                        )),
                    access == "delivery"
                        ? InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => AddDailyData()));
                            },
                            child: ListTile(
                              title: Text("Add Daily Data"),
                              leading: Icon(
                                Icons.library_books,
                                color: Color(0xff58b4ae),
                              ),
                            ),
                          )
                        : SizedBox.shrink(),
                    access == "owner"
                        ? InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => AddCustomer()));
                            },
                            child: ListTile(
                              title: Text("Add Customer"),
                              leading: Icon(
                                Icons.person_add,
                                color: Color(0xff58b4ae),
                              ),
                            ),
                          )
                        : SizedBox.shrink(),
                    access == "owner"
                        ? InkWell(
                            onTap: () async {
                              if (await ic.checkInternet()) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => ViewDetails()));
                              }
                              {
                                _alertBox.showAlertDialog(
                                    context,
                                    "No Internet",
                                    "Check your internet connection!");
                              }
                            },
                            child: ListTile(
                              title: Text("View Details"),
                              leading: Icon(
                                Icons.view_list,
                                color: Color(0xff58b4ae),
                              ),
                            ),
                          )
                        : SizedBox.shrink(),
                    access == "owner"
                        ? InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => ReportsScreen()));
                            },
                            child: ListTile(
                              title: Text("Reports"),
                              leading: Icon(
                                Icons.rate_review,
                                color: Color(0xff58b4ae),
                              ),
                            ),
                          )
                        : SizedBox.shrink(),
                    Divider(),
                    InkWell(
                      onTap: () {
                        FirebaseAuth.instance.signOut().then((value) {
                          Navigator.pushReplacement(context,
                              MaterialPageRoute(builder: (context) => Login()));
                        });
                      },
                      child: ListTile(
                        title: Text("Logout"),
                        leading: Icon(
                          Icons.power_settings_new,
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ],
                );
              }
              return Container();
            },
          ),
        ),
        body: Container(
            child: Center(child: Image.asset('assets/images/water.png'))));
  }
}
